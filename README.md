# Screening Task

Let’s say we have just implemented a new training algorithm for regression with the following interface:

```python
class NewComer:
    ...
    def train(self, x: List[List[float]], y: List[float]):
        ...
        
    def predict(self, x: List[float]) -> float:
        ...
    
        return 0
```

Now I have to design and write test suite for it in Python using `unittest` or `pytest` frameworks. So,
in this case I am going to use `unittest`.

So here the purpose of test suite is that we want to check that the code we are writing does what we expect.
so we write a sequence of assert statements checking different functions etc. then we will use these functions 
to check our code is ok. when we keep changing the code in the future, we can always use these to check our code
haven’t broken anything.

So here for the above interface I need to think about what out code should be doing, and then write some tests about 
it.

E.g. we could write something to check:
- a) the weights are changing when we train, 
- b) that the loss decreases, 
- c) that the prediction accuracy increases.

So the test-suite for the above interface will be something like this:

```python
import unittest


class TestNewComer(unittest.TestCase):

    def test_train_changes_weights(self):
        my_model = NewComer()
        original_weights = MyModel.weights.clone()
        x = foo # foo will be some numbers
        y = bar # bar will be some numbers
        my_model.train(x, y)
        new_weights = my_model.weights.clone()
        self.assertNotEqual(original_weights, new_weights)

    def test_train_improves_prediction_performance(self):
        my_model = NewComer()
        x = foo
        y = bar
        error_before_training = np.mean((y - my_model.predict(x)) ** 2)
        my_model.train(x, y)
        error_after_training = np.mean((y - my_model.predict(x)) ** 2)
        self.assertLessThan(error_after_training, error_before_training)
    
if __name__ == "__main__":
    unittest.main()
```

here we will choose small and easy values for `x` and `y`, not big datasets. Tests are meant to be 
easy and very fast to run. The idea is that we can quickly identify if some new commit or something 
has broken something. If we are going to have a lot of data that we don’t want to write in the script, 
we can read it from a file.