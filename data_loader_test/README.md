Now, I am explaining a real time example where using `unittest` I am going to create testing for a 
PyTorch data loader segmentation project. The full code is attached in the file `data_loader_test.py`.

So here the segmentation dataset being tested is supposed to load the corresponding image and mask pairs 
into batches. It is critical that the correct image is mapped to the correct mask. For this purpose, 
usually, both image and masks have the same number in their names. If we are resizing our image via some 
augmentations, then our resultant size should be as expected. For PyTorch, the tensor returned by the data 
loader should be of the form `BxCxHxW`, where `B` is the `batch size`, `C` is the number of `channels`, `H` 
is the `height` and `W` is the `width`.

Now, I’ll explain what’s happening in the code. I created a `Test_TestSegmentationDataset` class that inherits 
from the `unittest.TestCase` base class. Then I created a `setUpClass` method which is a class method to ensure 
that the initialization is done only once.

```python
class Test_TestSegmentationDataset(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        seg_dataset = SegmentationDataset("CrackForest",
                                          "Images",
                                          "Masks",
                                          transforms=transforms.Compose(
                                              [transforms.ToTensor()]))
        seg_dataloader = DataLoader(seg_dataset,
                                    batch_size=4,
                                    shuffle=False,
                                    num_workers=8)
        cls.samples = next(iter(seg_dataloader))
```

One thing to note here is, I’ve disabled the shuffling in dataloader for testing purpose. Since I would expect 
the `image` and `mask` with `001` in their names to be present at index `0` of the first batch created by the 
dataloader. Checking a different sample index from a different batch would be an even better test since I’ll be 
ensuring that the order is consistent across batches. I store the first batch in `cls.samples` as a class attribute.
Now that the initialization is done, we look at the individual tests.

In the first test, I check the `image` tensor dimensions returned by the `dataloader`. Since I’m not resizing the 
images I expect the size to be `320x480` and those images are being read as `RGB` so there should be `3` channels. 
In the `setUpClass` method, I specified the batch size to be `4`, so the first dimension of the tensor should be `4`. 
If there is something wrong with the dimensions, this test will fail.

```python
def test_image_tensor_dimensions(self):
        image_tensor_shape = Test_TestSegmentationDataset.samples[
            'image'].shape
        self.assertEqual(image_tensor_shape[0], 4)
        self.assertEqual(image_tensor_shape[1], 3)
        self.assertEqual(image_tensor_shape[2], 320)
        self.assertEqual(image_tensor_shape[3], 480)
```

The next test is exactly the same except that it is for the `mask` tensor. In this particular dataset, the masks have 
only one channel. So I expect the number of channels to be `1`. The batch size should be `4`. And the mask shape should be `320x480`.

```python
def test_mask_tensor_dimensions(self):
        mask_tensor_shape = Test_TestSegmentationDataset.samples['mask'].shape
        self.assertEqual(mask_tensor_shape[0], 4)
        self.assertEqual(mask_tensor_shape[1], 1)
        self.assertEqual(mask_tensor_shape[2], 320)
        self.assertEqual(mask_tensor_shape[3], 480)
```

The last test checks two things. First is whether the tensor obtained by applying the transforms specified in the dataloader manually yields the same result as the dataloader. And second is that the image and mask pairs are correct. To apply a torchvision transform directly, I need to instantiate the transform and pass my image as an input to that instance. If 
transform expects a PIL image or `numpy.array` (which is the case for `ToTensor`), any other format would result in an error.

```python
def test_mask_img_pair(self):
        ref_image_tensor = transforms.ToTensor()(Image.open(
            Path("CrackForest/Images/001.jpg")))
        ref_mask_tensor = transforms.ToTensor()(Image.open(
            Path("CrackForest/Masks/001_label.PNG")))
        datagen_image_tensor = Test_TestSegmentationDataset.samples['image'][0]
        datagen_mask_tensor = Test_TestSegmentationDataset.samples['mask'][0]
        self.assertTrue(torch.equal(ref_image_tensor, datagen_image_tensor))
        self.assertTrue(torch.equal(ref_mask_tensor, datagen_mask_tensor))
```

Now that I have my unittest ready, let us look at how to run this test via command line first.
I can use the following command:

`python -m unittest discover -s Tests -p "test_*"`

Now Unittest will discover my tests once I specify the search directory and the search pattern.