import unittest


class TestNewComer(unittest.TestCase):

    def test_train_changes_weights(self):
        my_model = NewComer()
        original_weights = MyModel.weights.clone()
        x = foo # foo will be some numbers
        y = bar # bar will be some numbers
        my_model.train(x, y)
        new_weights = my_model.weights.clone()
        self.assertNotEqual(original_weights, new_weights)

    def test_train_improves_prediction_performance(self):
        my_model = NewComer()
        x = foo
        y = bar
        error_before_training = np.mean((y - my_model.predict(x)) ** 2)
        my_model.train(x, y)
        error_after_training = np.mean((y - my_model.predict(x)) ** 2)
        self.assertLessThan(error_after_training, error_before_training)
    
if __name__ == "__main__":
    unittest.main()